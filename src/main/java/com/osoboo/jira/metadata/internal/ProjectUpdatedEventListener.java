package com.osoboo.jira.metadata.internal;

import java.util.Collection;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ProjectUpdatedEvent;
import com.osoboo.jira.metadata.JiraMetadata;
import com.osoboo.jira.metadata.MetadataService;


public class ProjectUpdatedEventListener implements InitializingBean, DisposableBean {

    private EventPublisher eventPublisher;
    private MetadataService metadataService;


    public ProjectUpdatedEventListener(EventPublisher eventPublisher, MetadataService metadataService) {
        this.eventPublisher = eventPublisher;
        this.metadataService = metadataService;
    }


    /**
     * Called when the plugin has been enabled.
     * 
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }


    /**
     * Called when the plugin is being disabled or removed.
     * 
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }


    @EventListener
    public void onProjectUpdatedEvent(ProjectUpdatedEvent projectUpdatedEvent) {
        if (projectUpdatedEvent != null && projectUpdatedEvent.getOldProject() != null
                && projectUpdatedEvent.getProject() != null) {
            if (projectUpdatedEvent.getOldProject().getKey() != null
                    && !projectUpdatedEvent.getOldProject().getKey().equals(
                            projectUpdatedEvent.getProject().getKey())) {
                Collection<JiraMetadata> metadatas = metadataService.getMetadata(projectUpdatedEvent
                        .getOldProject());
                for (JiraMetadata metadata : metadatas) {
                    metadataService.save(projectUpdatedEvent.getProject(), metadata.getUserKey(), metadata
                            .getUserValue(), metadata.getUserGrouping(), metadata.isHidden());
                    metadataService.delete(projectUpdatedEvent.getOldProject(), metadata.getID());
                }
            }

        }
    }
}