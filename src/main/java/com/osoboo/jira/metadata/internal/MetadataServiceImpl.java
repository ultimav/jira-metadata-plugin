/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.internal;

import com.atlassian.jira.user.ApplicationUser;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.osoboo.jira.metadata.JiraMetadata;
import com.osoboo.jira.metadata.MetadataService;
import net.java.ao.Query;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;


public class MetadataServiceImpl implements MetadataService {

    private final ActiveObjects ao;
    @SuppressWarnings("rawtypes") private Map<Class, EnrichedObjectKeyGenerator> registeredGenerators = new HashMap<Class, EnrichedObjectKeyGenerator>();


    public MetadataServiceImpl(ActiveObjects ao) {
        this.ao = ao;
        registerKeyGenerator(new KeyGeneratorProject());
        registerKeyGenerator(new KeyGeneratorVersion());
        registerKeyGenerator(new KeyGeneratorComponent());
        registerKeyGenerator(new KeyGeneratorUser());
        registerKeyGenerator(new KeyGeneratorGroup());
    }


    private void registerKeyGenerator(EnrichedObjectKeyGenerator generator) {
        registeredGenerators.put(generator.getClassIdentifier(), generator);
    }


    @Override
    public void save(Object enrichedObject, String key, String value) {
        save(enrichedObject, key, value, false);
    }


    /** {@inheritDoc} */
    @Override
    public void save(Object enrichedObject, String key, String value, boolean hidden) {
        save(enrichedObject, key, value, null, hidden);
    }


    @Override
    public void save(Object enrichedObject, String key, String value, String group, boolean hidden) {
        String enrichedObjectKey = getEnrichedObjectKey(enrichedObject);
        if (enrichedObjectKey == null) {
            return;
        }
        JiraMetadata metadata = null;
        JiraMetadata[] result = ao.find(JiraMetadata.class, "ENRICHED_OBJECT_KEY = ? AND USER_KEY = ?",
                enrichedObjectKey, key);
        if (result.length > 0) {
            metadata = result[0];
        } else {
            metadata = ao.create(JiraMetadata.class);
            metadata.setEnrichedObjectKey(enrichedObjectKey);
            metadata.setUserKey(key);
        }
        metadata.setUserValue(value);
        metadata.setUserGrouping(group);
        metadata.setHidden(hidden);
        metadata.save();
    }


    /** {@inheritDoc} */
    @Override
    public String getMetadataValue(Object enrichedObject, String key) {
        JiraMetadata metadata = getAllMetadata(enrichedObject, key);
        if (metadata == null) {
            return "";
        }
        return metadata.getUserValue();
    }


    /** {@inheritDoc} */
    @Override
    public Collection<JiraMetadata> getAllMetadata(Object enrichedObject) {
        String enrichedObjectKey = getEnrichedObjectKey(enrichedObject);
        try {
            JiraMetadata[] result = ao.find(JiraMetadata.class, Query.select().where(
                    "ENRICHED_OBJECT_KEY = ?", enrichedObjectKey));
            Collection<JiraMetadata> resultSet = new TreeSet<JiraMetadata>(new JiraMetadataComparator());
            for (JiraMetadata metadata : result) {
                resultSet.add(metadata);
            }
            return resultSet;
        } catch (Exception e) {
            return Collections.<JiraMetadata> emptyList();
        }
    }


    /** {@inheritDoc} */
    @Override
    public Collection<JiraMetadata> getMetadata(String enrichedClassIdentifierAsString, String userKey, String userValue) {
        try {
            JiraMetadata[] result = ao.find(JiraMetadata.class, Query.select().where(
                    "ENRICHED_OBJECT_KEY like ? AND USER_KEY = ? AND USER_VALUE = ?", enrichedClassIdentifierAsString + "%", userKey, userValue));
            Collection<JiraMetadata> resultSet = new TreeSet<JiraMetadata>(new JiraMetadataComparator());
            for (JiraMetadata metadata : result) {
                if (!metadata.isHidden()) {
                    resultSet.add(metadata);
                }
            }
            return resultSet;
        } catch (Exception e) {
            return Collections.<JiraMetadata> emptyList();
        }
    }


    /** {@inheritDoc} */
    @Override
    public Collection<JiraMetadata> getMetadata(Object enrichedObject) {
        String enrichedObjectKey = getEnrichedObjectKey(enrichedObject);
        try {
            JiraMetadata[] result = ao.find(JiraMetadata.class, Query.select().where(
                    "ENRICHED_OBJECT_KEY = ?", enrichedObjectKey));
            Collection<JiraMetadata> resultSet = new TreeSet<JiraMetadata>(new JiraMetadataComparator());
            for (JiraMetadata metadata : result) {
                if (!metadata.isHidden()) {
                    resultSet.add(metadata);
                }
            }
            return resultSet;
        } catch (Exception e) {
            return Collections.<JiraMetadata> emptyList();
        }
    }


    /** {@inheritDoc} */
    @Override
    public void delete(Object enrichedObject, int id) {
        String enrichedObjectKey = getEnrichedObjectKey(enrichedObject);
        if (enrichedObjectKey == null) {
            return;
        }
        JiraMetadata[] result = ao.find(JiraMetadata.class, "ENRICHED_OBJECT_KEY = ? AND ID = ?",
                enrichedObjectKey, id);
        ao.delete(result);
    }


    /**
     * @param enrichedObject
     *            the enriced object
     * @param key
     *            the metadata key
     * @return the metadata object
     */
    public JiraMetadata getMetadata(Object enrichedObject, String key) {
        JiraMetadata metadata = getAllMetadata(enrichedObject, key);
        if (metadata != null && !metadata.isHidden()) {
            return metadata;
        }
        return null;
    }


    /**
     * @param enrichedObject
     *            the enriced object
     * @param key
     *            the metadata key
     * @return the metadata object
     */
    private JiraMetadata getAllMetadata(Object enrichedObject, String key) {
        String enrichedObjectKey = getEnrichedObjectKey(enrichedObject);
        JiraMetadata[] result = ao.find(JiraMetadata.class, "ENRICHED_OBJECT_KEY = ? AND USER_KEY = ?",
                enrichedObjectKey, key);
        if (result.length == 0) {
            result = ao.find(JiraMetadata.class, "ENRICHED_OBJECT_KEY = ? AND USER_KEY = ?",
                    enrichedObjectKey, key);
        }
        if (result.length > 0) {
            return result[0];
        }
        return null;
    }


    /**
     * @param enrichedObject
     * @return the generated object key will be returned
     */
    private String getEnrichedObjectKey(Object enrichedObject) {
        if (enrichedObject != null && registeredGenerators.containsKey(enrichedObject.getClass())) {
            return registeredGenerators.get(enrichedObject.getClass()).getKey(enrichedObject);
        }

        if (User.class.isInstance(enrichedObject)) {
            return registeredGenerators.get(User.class).getKey(enrichedObject);
        }
        if (ApplicationUser.class.isInstance(enrichedObject)) {
            return registeredGenerators.get(User.class).getKey(enrichedObject);
        }
        if (Group.class.isInstance(enrichedObject)) {
            return registeredGenerators.get(Group.class).getKey(enrichedObject);
        }
        return null;
    }

    class JiraMetadataComparator implements Comparator<JiraMetadata> {
        @Override
        public int compare(JiraMetadata o1, JiraMetadata o2) {
            try {
                if (o1.getUserGrouping() == null && o2.getUserGrouping() != null) {
                    return -1;
                }
                if (o1.getUserGrouping() != null && o2.getUserGrouping() == null) {
                    return 1;
                }
                int result = o1.getUserGrouping().compareTo(o2.getUserGrouping());
                if (result != 0) {
                    return result;
                }
                return o1.getUserKey().compareTo(o2.getUserKey());
            } catch (Exception e) {
                try {
                    return o1.getUserKey().compareTo(o2.getUserKey());
                } catch (Exception ex) {
                    return 0;
                }
            }
        }
    }

}
