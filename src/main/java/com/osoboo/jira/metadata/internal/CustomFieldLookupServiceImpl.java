package com.osoboo.jira.metadata.internal;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.osoboo.jira.metadata.CustomFieldLookupService;
import com.osoboo.jira.metadata.ui.field.MetadataCalcFieldType;

public class CustomFieldLookupServiceImpl implements CustomFieldLookupService {

	private static final Logger log = Logger.getLogger(CustomFieldLookupServiceImpl.class);

	CustomFieldManager customFieldManager;

	public CustomFieldLookupServiceImpl(CustomFieldManager customFieldManager) {
		this.customFieldManager = customFieldManager;
	}

	@Override
	public Object getCustomFieldValue(Issue issue, String customFieldId) {
		try {
			CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
			if (customField.getCustomFieldType() instanceof MetadataCalcFieldType) {
				return null;
			}
			return issue.getCustomFieldValue(customField);
		} catch (RuntimeException e) {
			log.info("RuntimeException while CustomFieldValue lookup returning null");
			return null;
		}
	}

}
