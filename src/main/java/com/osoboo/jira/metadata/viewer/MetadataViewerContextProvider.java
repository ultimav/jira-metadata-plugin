package com.osoboo.jira.metadata.viewer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.TemplateSources;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.MetadataService;

/**
 * the context provider to get the contact data of the assignee and reporter.
 * 
 * @author aspall
 * 
 */
public class MetadataViewerContextProvider implements ContextProvider {

    private static final Logger logger = Logger.getLogger(MetadataViewerContextProvider.class);
	private UserUtil userUtil;
    private final WebFragmentHelper webFragmentHelper;
	private MetadataService metadataService;
	private ProjectRoleManager projectRoleManager;
	private JiraAuthenticationContext jiraAuthenticationContext;
	private ApplicationProperties applicationProperties;

    public MetadataViewerContextProvider(UserUtil userUtil, MetadataService metadataService, JiraAuthenticationContext jiraAuthenticationContext, ProjectRoleManager projectRoleManager, ApplicationProperties applicationProperties, WebFragmentHelper webFragmentHelper) {
		this.userUtil = userUtil;
		this.metadataService = metadataService;
		this.jiraAuthenticationContext = jiraAuthenticationContext;
		this.projectRoleManager = projectRoleManager;
		this.applicationProperties = applicationProperties;
        this.webFragmentHelper = webFragmentHelper;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }
    

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        try {
            VelocityTemplatingEngine renderer = ComponentAccessor.getComponent(VelocityTemplatingEngine.class);
            Collection<LabelValue> labelValue = new ArrayList<MetadataViewerContextProvider.LabelValue>();
            Issue currentIssue = (Issue) context.get("issue");

            final Map<String, Object> params = new HashMap<String, Object>();
            params.put("metadataService", metadataService);
            params.put("textutils", new TextUtils());
            params.put("userutil", userUtil);
            params.put("helper", new Helper(userUtil, metadataService));
            params.put("issue", currentIssue);
            String metadataValue = metadataService.getMetadataValue(currentIssue.getProjectObject(), "meta.view");
            StringTokenizer stringTokenizer = new StringTokenizer(metadataValue, ";");
            String metadataViewLabel = metadataService.getMetadataValue(currentIssue.getProjectObject(), "meta.view.label");
            while (stringTokenizer.hasMoreTokens()) {
            	String metadataKey = stringTokenizer.nextToken();
            	String visibleTo = metadataService.getMetadataValue(currentIssue.getProjectObject(), metadataKey + ".visibleTo");
            	if (visibleTo != null && !visibleTo.isEmpty()) {
            		ProjectRole role = projectRoleManager.getProjectRole(visibleTo);
            		if (role != null && !projectRoleManager.isUserInProjectRole(jiraAuthenticationContext.getLoggedInUser(), role, currentIssue.getProjectObject())) {
            			continue;
            		}
            	}
            	String label = metadataService.getMetadataValue(currentIssue.getProjectObject(), metadataKey + ".label");
                String value = webFragmentHelper.renderVelocityFragment(metadataService.getMetadataValue(currentIssue.getProjectObject(), metadataKey + ".value"), params);
            	labelValue.add(new LabelValue(label, value));
            }
            context.put("textutils", new TextUtils());
            context.put("forceHtmlEncode", applicationProperties.getOption("metadata.view.forceHtmlEncode"));
            context.put("labelValues", labelValue);
            if (metadataViewLabel != null && !metadataViewLabel.isEmpty()) {
            	context.put("title", metadataViewLabel);
            }
        } catch (Exception e) {
            logger.info("something went wrong while phonenumber lookup for profileUser " + context.get("profileUser"));
            if (logger.isDebugEnabled()) {
                logger.debug("the error message", e);
            }
        }
        return context;
    }

    public static class LabelValue implements Serializable {
		private static final long serialVersionUID = 6472065078012594512L;
		private String label;
    	private String value;
		public LabelValue() {
			super();
		}
		public LabelValue(String label, String value) {
			super();
			this.label = label;
			this.value = value;
		}
		@HtmlSafe
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		@HtmlSafe
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
    }
}
