package com.osoboo.jira.metadata.viewer;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.osoboo.jira.metadata.ui.internal.MetadataHelper;


/**
 * Hier ist das Admin Servlet
 */
public class AdminServlet extends HttpServlet {

    /** */
    private static final long serialVersionUID = 8141757324682992130L;

    private final TemplateRenderer renderer;
    private final LoginUriProvider loginUriProvider;
    private final ApplicationProperties applicationProperties;

    private JiraAuthenticationContext jiraAuthenticationContext;

    private PermissionManager permissionManager;


    public AdminServlet(JiraAuthenticationContext jiraAuthenticationContext,
            PermissionManager permissionManager, LoginUriProvider loginUriProvider,
            TemplateRenderer renderer, ApplicationProperties applicationProperties) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.loginUriProvider = loginUriProvider;
        this.renderer = renderer;
        this.applicationProperties = applicationProperties;
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (!MetadataHelper.userHasAdminPermission(permissionManager, jiraAuthenticationContext)) {
            redirectToLogin(request, response);
            return;
        }

        response.setContentType("text/html;charset=utf-8");
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("forceHtmlEncode", applicationProperties.getOption("metadata.view.forceHtmlEncode"));
        renderer.render("templates/plugins/jira/metadata/admin.vm", context, response.getWriter());
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        if (!MetadataHelper.userHasAdminPermission(permissionManager, jiraAuthenticationContext)) {
            redirectToLogin(request, response);
            return;
        }
        applicationProperties.setOption("metadata.view.forceHtmlEncode", Boolean.parseBoolean(request
                .getParameter("forceHtmlEncode")));
        response.setContentType("text/html;charset=utf-8");
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("forceHtmlEncode", applicationProperties.getOption("metadata.view.forceHtmlEncode"));
        renderer.render("templates/plugins/jira/metadata/admin.vm", context, response.getWriter());
    }


    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }


    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}