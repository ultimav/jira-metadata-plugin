package com.osoboo.jira.metadata.viewer;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.util.UserUtil;
import com.osoboo.jira.metadata.MetadataService;

public class Helper {

	private UserUtil userUtil;
	private MetadataService metadataService;

	public Helper(UserUtil userUtil, MetadataService metadataService) {
		super();
		this.userUtil = userUtil;
		this.metadataService = metadataService;
	}

	public String getMetadataValueFromFirstGroup(User user, String groupStartsWith,
			String key) {
		for (String groupName : userUtil.getGroupNamesForUser(user.getName())) {
			if (groupName.startsWith(groupStartsWith)) {
				Group group = userUtil.getGroupObject(groupName);
				return metadataService.getMetadataValue(group, key);
			}
		}
		return "";
	}

}
