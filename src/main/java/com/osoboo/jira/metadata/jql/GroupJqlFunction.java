package com.osoboo.jira.metadata.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.internal.KeyGeneratorGroup;


public class GroupJqlFunction extends AbstractMetadataJqlFunction {

    private String classIdentifierAsString;


    public GroupJqlFunction(MetadataService metadataService) {
        super(metadataService);
        this.classIdentifierAsString = new KeyGeneratorGroup().getClassIdentifierAsString();
    }


    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.GROUP;
    }


    @Override
    public String getFunctionName() {
        return "groupsMetadata";
    }


    @Override
    protected String getClassIdentifierAsString() {
        return classIdentifierAsString;
    }
}
