package com.osoboo.jira.metadata.jql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.osoboo.jira.metadata.JiraMetadata;
import com.osoboo.jira.metadata.MetadataService;

import java.util.LinkedList;
import java.util.List;


public abstract class AbstractMetadataJqlFunction extends AbstractJqlFunction {

    private MetadataService metadataService;


    public AbstractMetadataJqlFunction(MetadataService metadataService) {
        this.metadataService = metadataService;
    }


    protected abstract String getClassIdentifierAsString();


    @Override
    public boolean isList() {
        return true;
    }


    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 2;
    }


    @Override
    public List<QueryLiteral> getValues(QueryCreationContext arg0, FunctionOperand operand, TerminalClause arg2) {
        final List<QueryLiteral> literals = new LinkedList<QueryLiteral>();
        final List<String> arguments = operand.getArgs();
        final String userKey = arguments.get(0);
        final String metaValue = arguments.get(1);
        for (JiraMetadata metadata : metadataService.getMetadata(getClassIdentifierAsString(), userKey, metaValue)) {
            String projectKey = metadata.getEnrichedObjectKey().replace(getClassIdentifierAsString() + ":", "");
            literals.add(new QueryLiteral(operand, projectKey));
        }
        return literals;
    }

    @Override
    public MessageSet validate(User user, FunctionOperand operand, TerminalClause clause) {
        return validateNumberOfArgs(operand, getMinimumNumberOfExpectedArguments());
    }

}
