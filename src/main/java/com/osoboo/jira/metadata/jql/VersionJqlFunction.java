package com.osoboo.jira.metadata.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.internal.KeyGeneratorVersion;


public class VersionJqlFunction extends AbstractMetadataJqlFunction {

    private String classIdentifierAsString;


    public VersionJqlFunction(MetadataService metadataService) {
        super(metadataService);
        this.classIdentifierAsString = new KeyGeneratorVersion().getClassIdentifierAsString();
    }


    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.VERSION;
    }


    @Override
    public String getFunctionName() {
        return "versionsMetadata";
    }


    @Override
    protected String getClassIdentifierAsString() {
        return classIdentifierAsString;
    }

}
