package com.osoboo.jira.metadata;

public class MetadataUtils {
    
    public String transformHtmlSaveToId(String id) {
        // Punctuation: One of !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
        return id.replaceAll(" |\\p{Punct}", "_");
    }
    
    public static void main(String[] args) {
        MetadataUtils utils = new MetadataUtils();
        System.out.println(utils.transformHtmlSaveToId("a.b:c|d e\\f'g\"h"));
    }

}
