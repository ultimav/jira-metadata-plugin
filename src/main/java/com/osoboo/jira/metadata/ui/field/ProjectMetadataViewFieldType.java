/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.field;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.searchers.ExactNumberSearcher;
import com.atlassian.jira.issue.customfields.searchers.NumberRangeSearcher;
import com.atlassian.jira.issue.customfields.searchers.TextSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.osoboo.jira.metadata.MetadataService;

@SuppressWarnings("unchecked")
public class ProjectMetadataViewFieldType extends AbstractMetadataCFType {

	private final MetadataService metadataService;
	private static final Logger log = Logger.getLogger(ProjectMetadataViewFieldType.class);

	protected ProjectMetadataViewFieldType(GenericConfigManager genericConfigManager,
			MetadataService metadataService) {
		super(genericConfigManager);
		this.metadataService = metadataService;
	}

	@Override
	public Object getValueFromIssue(CustomField customField, Issue issue) {
		String metadataValue = metadataService.getMetadataValue(issue.getProjectObject(),
				(String) getDefaultValue(customField.getRelevantConfig(issue)));
		if (customField.getCustomFieldSearcher() instanceof TextSearcher) {
			return metadataValue;
		}
		try {
			return Double.parseDouble(metadataValue);
		} catch (NumberFormatException e) {
			if (customField.getCustomFieldSearcher() instanceof ExactNumberSearcher
					|| customField.getCustomFieldSearcher() instanceof NumberRangeSearcher) {
				log.warn(MessageFormat
						.format("the custom field {1} of the issue {0} has been configured to use the wrong field searcher but the value \"{2}\" can't be transformed into a Number i will return 0.0 to avoid an internal jira error. please correct the metadata to return a number or change the field searcher to text searcher",
								issue.getKey(), customField.getName(), metadataValue));
				return Double.valueOf(0.0);
			}
			return metadataValue;
		}
	}

}
