package com.osoboo.jira.metadata.ui.internal;

import java.util.Map;

import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.plugin.componentpanel.BrowseComponentContext;
import com.atlassian.jira.plugin.componentpanel.impl.GenericTabPanel;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.MetadataUtils;

public class ProjectComponentMetadataPanel extends GenericTabPanel {

	private MetadataService metadataService;
	/** used to check the current users permission to edit the metadata. */
	private final PermissionManager permissionManager;
	private final ProjectRoleManager projectRoleManager;

	public ProjectComponentMetadataPanel(final ProjectManager projectManager, final JiraAuthenticationContext authenticationContext, final SearchProvider searchProvider,
			final FieldVisibilityManager fieldVisibilityManager, PermissionManager permissionManager, MetadataService metadataService,
			ProjectRoleManager projectRoleManager) {
		super(projectManager, authenticationContext, fieldVisibilityManager);
		this.permissionManager = permissionManager;
		this.metadataService = metadataService;
		this.projectRoleManager = projectRoleManager;
	}

	@Override
	protected Map<String, Object> createVelocityParams(BrowseComponentContext context) {
		final Map<String, Object> params = JiraVelocityUtils.getDefaultVelocityParams(super.createVelocityParams(context),
				authenticationContext);
		params.put("i18n", authenticationContext.getI18nHelper());
		params.put("projectComponent", context.getComponent());
		params.put("hasAdminPermission", MetadataHelper.userHasProjectAdminPermission(permissionManager, authenticationContext, context));
		params.put("metadataService", metadataService);
		params.put("textutils", new TextUtils());
        params.put("metadataUtils", new MetadataUtils());
		return params;

	}

	@Override
	public boolean showPanel(BrowseComponentContext ctx) {
		String hideRoles = metadataService.getMetadataValue(ctx.getComponent(), "jira.metadata.projectRole.hide.tab.component");
		return MetadataHelper.userIsNotInRole(projectRoleManager, permissionManager, authenticationContext, ctx, hideRoles);
	}

}
