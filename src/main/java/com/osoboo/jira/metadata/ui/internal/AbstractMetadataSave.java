package com.osoboo.jira.metadata.ui.internal;

import org.apache.commons.lang.StringUtils;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;

/**
 * the common save values for e.g project, version related metadata.
 */
@SuppressWarnings("unchecked")
public abstract class AbstractMetadataSave extends JiraWebActionSupport {

	/** */
	private static final long serialVersionUID = -8286243063136749937L;

	private String metadataKey;

	private String metadataValue;

	private String metadataGroup;

	private boolean metadataHidden;

	protected final MetadataService metadataService;

	protected final PermissionManager permissionManager;

	protected final JiraAuthenticationContext jiraAuthenticationContext;

	public AbstractMetadataSave(PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext,
			MetadataService metadataService) {
		this.permissionManager = permissionManager;
		this.jiraAuthenticationContext = jiraAuthenticationContext;
		this.metadataService = metadataService;
	}

	public String getMetadataKey() {
		return StringUtils.abbreviate(metadataKey, 255);
	}

	public void setMetadataKey(String metadataKey) {
		this.metadataKey = metadataKey;
	}

	public String getMetadataValue() {
		return StringUtils.abbreviate(metadataValue, 255);
	}

	public void setMetadataValue(String metadataValue) {
		this.metadataValue = metadataValue;
	}

	public boolean isMetadataHidden() {
		return metadataHidden;
	}

	public void setMetadataHidden(boolean metadataHidden) {
		this.metadataHidden = metadataHidden;
	}

	/**
	 * @return the metadataGroup
	 */
	public String getMetadataGroup() {
		return StringUtils.abbreviate(metadataGroup, 255);
	}

	/**
	 * @param metadataGroup
	 *            the metadataGroup to set
	 */
	public void setMetadataGroup(String metadataGroup) {
		this.metadataGroup = metadataGroup;
	}

}
