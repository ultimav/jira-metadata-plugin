/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import java.text.MessageFormat;

import webwork.action.ServletActionContext;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;

public class VersionMetadataDelete extends JiraWebActionSupport {

	/** */
	private static final long serialVersionUID = 7861762358377860485L;

	private int metadataKey;

	private long versionId;

	private final MetadataService metadataService;

	private final PermissionManager permissionManager;

	private final JiraAuthenticationContext jiraAuthenticationContext;

	public VersionMetadataDelete(PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext,
			MetadataService metadataService) {
		this.permissionManager = permissionManager;
		this.jiraAuthenticationContext = jiraAuthenticationContext;
		this.metadataService = metadataService;
	}

	public int getMetadataKey() {
		return metadataKey;
	}

	public void setMetadataKey(int metadataKey) {
		this.metadataKey = metadataKey;
	}

	public long getVersionId() {
		return versionId;
	}

	public void setVersionId(long versionId) {
		this.versionId = versionId;
	}

	@Override
	protected String doExecute() throws Exception {
		Version version = getVersionManager().getVersion(versionId);
		Project project = version.getProjectObject();

		if (MetadataHelper.userHasProjectAdminPermission(permissionManager, jiraAuthenticationContext, project)) {
			metadataService.delete(version, metadataKey);
		} else {
			log.warn(MessageFormat.format(
					"The user {0} tried to delete the metadata with key {1} for the version {2} / project {3} - i'll ignore the call",
					getLoggedInUser().getName(), metadataKey, version.getName(), project.getKey()));
		}
		StringBuilder sb = new StringBuilder();
		sb.append(ServletActionContext.getRequest().getContextPath());
		sb.append("/browse/");
		sb.append(version.getProjectObject().getKey());
		sb.append("/fixforversion/");
		sb.append(version.getId());
		ServletActionContext.getResponse().sendRedirect(sb.toString());
		return NONE;
	}
}
