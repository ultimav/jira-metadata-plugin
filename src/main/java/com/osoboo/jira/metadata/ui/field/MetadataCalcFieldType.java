/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.field;

import java.text.MessageFormat;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.MathTool;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.searchers.ExactNumberSearcher;
import com.atlassian.jira.issue.customfields.searchers.NumberRangeSearcher;
import com.atlassian.jira.issue.customfields.searchers.TextSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.util.velocity.NumberTool;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.CustomFieldLookupService;
import com.osoboo.jira.metadata.MetadataService;

@SuppressWarnings("unchecked")
public class MetadataCalcFieldType extends AbstractMetadataCFType {

	private final WebFragmentHelper webFragmentHelper;
	private MetadataService metadataService;
	private CustomFieldLookupService customFieldLookupService;
	private static final Logger log = Logger.getLogger(MetadataCalcFieldType.class);

	protected MetadataCalcFieldType(GenericConfigManager genericConfigManager, JiraAuthenticationContext authenticationContext,
			WebFragmentHelper webFragmentHelper, MetadataService metadataService, CustomFieldLookupService customFieldLookupService) {
		super(genericConfigManager);
		this.webFragmentHelper = webFragmentHelper;
		this.metadataService = metadataService;
		this.customFieldLookupService = customFieldLookupService;
	}

	/**
	 * renders the fields default value as velocity fragment. (the velocity
	 * fragment defined within the default value can use the current issue,
	 * current customfield, my metadata service, my custom field lookup service,
	 * the velocity math tools, the jira number tool and the opensymphony text
	 * utils.
	 */
	@Override
	public Object getValueFromIssue(CustomField customField, Issue issue) {
		try {
			final Map<String, Object> startingParams = JiraVelocityUtils.getDefaultVelocityParams(ComponentManager.getInstance()
					.getJiraAuthenticationContext());
			startingParams.put("issue", issue);
			startingParams.put("customField", customField);
			startingParams.put("metadataService", metadataService);
			startingParams.put("math", new MathTool());
			startingParams.put("numberTool", new NumberTool(getI18nBean().getLocale()));
			startingParams.put("textutils", new TextUtils());
			startingParams.put("customFieldLookupService", customFieldLookupService);
			String calcMetadataValue = webFragmentHelper.renderVelocityFragment(getDefaultValue(customField.getRelevantConfig(issue))
					.toString(), startingParams);
			if (customField.getCustomFieldSearcher() instanceof TextSearcher) {
				return calcMetadataValue;
			}
			try {
				return Double.parseDouble(calcMetadataValue);
			} catch (NumberFormatException e) {
				if (customField.getCustomFieldSearcher() instanceof ExactNumberSearcher
						|| customField.getCustomFieldSearcher() instanceof NumberRangeSearcher) {
					log.warn(MessageFormat
							.format("the custom field {1} of the issue {0} has been configured to use the wrong field searcher but the value \"{2}\" can't be transformed into a Number i will return 0.0 to avoid an internal jira error. please correct the value calculation to return a number or change the field searcher to text searcher",
									issue.getKey(), customField.getName(), calcMetadataValue));
					return Double.valueOf(0.0);
				}
			}
			return calcMetadataValue;
		} catch (Exception e) {
			if (customField.getCustomFieldSearcher() instanceof ExactNumberSearcher
					|| customField.getCustomFieldSearcher() instanceof NumberRangeSearcher) {
				return Double.valueOf(0.0);
			}
			return "";
		}
	}

}
