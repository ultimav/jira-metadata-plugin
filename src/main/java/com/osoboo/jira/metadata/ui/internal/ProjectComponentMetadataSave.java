/*
 * Copyright (c) 2013, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import java.text.MessageFormat;

import webwork.action.ServletActionContext;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.osoboo.jira.metadata.MetadataService;

@SuppressWarnings("unchecked")
public class ProjectComponentMetadataSave extends AbstractMetadataSave {

	/** */
	private static final long serialVersionUID = 7861762358377860485L;

	private long projectComponentId;

	private ProjectComponentManager projectComponentManager;

	public ProjectComponentMetadataSave(PermissionManager permissionManager,
			JiraAuthenticationContext jiraAuthenticationContext,
			MetadataService metadataService,
			ProjectComponentManager projectComponentManager) {
		super(permissionManager, jiraAuthenticationContext, metadataService);
		this.projectComponentManager = projectComponentManager;
	}

	public long getProjectComponentId() {
		return projectComponentId;
	}

	public void setProjectComponentId(long projectComponentId) {
		this.projectComponentId = projectComponentId;
	}

	@Override
	protected String doExecute() throws Exception {
		ProjectComponent projectComponent = projectComponentManager.find(projectComponentId);
		Project project = getProjectManager().getProjectObj(projectComponent.getProjectId());

		if (MetadataHelper.userHasProjectAdminPermission(permissionManager,
				jiraAuthenticationContext, project)) {
			metadataService.save(projectComponent, getMetadataKey(), getMetadataValue(),
					getMetadataGroup(), isMetadataHidden());
		} else {
			log.warn(MessageFormat
					.format("The user {0} tried to edit the metadata with key {1} for the component {2} / project {3} - i'll ignore the call",
							getLoggedInUser().getName(), getMetadataKey(),
							projectComponent.getName(), project.getKey()));
		}

		StringBuilder sb = new StringBuilder();
		sb.append(ServletActionContext.getRequest().getContextPath());
		sb.append("/browse/");
		sb.append(project.getKey());
		sb.append("/component/");
		sb.append(projectComponent.getId());
		ServletActionContext.getResponse().sendRedirect(sb.toString());
		return NONE;
	}
}
