package com.osoboo.jira.metadata;

import org.junit.Test;
import org.openqa.selenium.By;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class UserProfileTest extends AbstractMetadataTestCase {
    
    @Override
    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("base_with_metadata_view_2014_04_21.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }
    
    @Test
    public void testShowAdminDetails() {
        gotoUrl("/secure/ViewProfile.jspa");
        waitForElement(By.id("content"));
        assertTextPresent("Further Details");
        assertTextPresent("phone");
        assertTextPresent("555-598-587");
        
        gotoUrl("/secure/ViewProfile.jspa?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-user-tab");
        waitForElement(By.id("content"));
        assertTextPresent("Your personal Metadata");
        assertTextPresent("phone");
        assertTextPresent("555-598-587");
        assertTextPresent("switch to admin interface");        
    }
    
    @Test
    public void testChangePhone() {
        gotoUrl("/secure/ViewProfile.jspa?selectedTab=jira.user.profile.panels:user-profile-summary-panel");
        waitForElement(By.id("content"));
        assertTextPresent("Further Details");
        assertTextPresent("phone");
        assertTextPresent("555-598-587");

        gotoUrl("/secure/ViewProfile.jspa?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-user-tab");
        waitAndClick(By.id("edit-phone"));
        setFormElement(By.id("metadataValue"), "555-598-777");
        click(By.id("save_metadata"));

        gotoUrl("/secure/ViewProfile.jspa?selectedTab=jira.user.profile.panels:user-profile-summary-panel");
        waitForElement(By.id("content"));
        assertTextPresent("Further Details");
        assertTextPresent("phone");
        assertTextPresent("555-598-777");
    }
    
    @Test
    public void testViewOthersMetadata() {
        jira.logout();
        jira.gotoLoginPage().loginAndGoToHome("test", "test");

        gotoUrl("/secure/ViewProfile.jspa");
        waitForElement(By.id("content"));
        assertTextPresent("Weitere Informationen");
        assertTextPresent("phone");
        assertTextPresent("555-598-588");
        assertTextPresent("Metadaten");

        gotoUrl("/secure/ViewProfile.jspa?name=admin");
        waitForElement(By.id("content"));
        assertTextPresent("Weitere Informationen");
        assertTextPresent("phone");
        assertTextPresent("555-598-587");
        assertTextNotPresent("Metadaten");
}
}
