package com.osoboo.jira.metadata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ConfigureFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.TypeSelectionCustomFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ViewCustomFields;
import com.atlassian.webdriver.utils.element.ElementIsVisible;

@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class MyPluginTest extends AbstractMetadataTestCase {

    protected boolean shouldSkipSetup() {
        return true;
    }

	public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("base_with_project_2011_11_24.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
	}

	/**
	 * error messages.
	 */
	@Test
	public void testUsersPermissions() {
		jira.logout();
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
		try {
			assertTextPresent("You must log in to access this page.");
		} catch (Throwable e) {
			assertTextPresent("Du musst angemeldet sei");
		}

		jira.gotoLoginPage().loginAndGoToHome("admin", "admin");

		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
		assertTextPresent("Metadata");
		assertTextPresent("You can create a new metadata or manage an existing one.");
		assertTextPresent("Edit");

		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
		assertTextNotPresent("You can create a new metadata or manage an existing one.");
		assertTextNotPresent("Edit");

	}

	@Test
	public void testCreateEditDeleteMetadata() {
		// create metadata
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
		
        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "create-edit-delete");
        setFormElement(By.id("metadataValue"), "some value");
        click(By.id("save_metadata"));
        waitForElement(By.id("content"));
        assertTextPresent("create-edit-delete");
        assertTextPresent("some value");

		// edit metadata
        waitAndClick(By.id("edit-create_edit_delete"));
        setFormElement(By.id("metadataValue"), "some other value");
        click(By.id("save_metadata"));
        waitForElement(By.id("content"));
        assertTextPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextPresent("some other value");

		// delete metadata
		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("some other value");
        assertFalse(jira.getTester().getDriver().elementExists(By.id("delete-create_edit_delete")));

		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitAndClick(By.id("delete-create_edit_delete"));
        jira.getTester().getDriver().switchTo().alert().accept();

        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitForElement(By.id("jira-metadata"));
		assertTextNotPresent("create-edit-delete");
		assertTextNotPresent("some value");
		assertTextNotPresent("some other value");
	}

	@Test
	public void testHiddenMetadata() {
		// create hour rate
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
		setFormElement(By.id("metadataKey"), "accounting.hour.rate");
		setFormElement(By.id("metadataValue"), "56.34");
		click(By.id("save_metadata"));
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.hour.rate");
		assertTextPresent("56.34");

		// create accounting code
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
		setFormElement(By.id("metadataKey"), "accounting.code");
		setFormElement(By.id("metadataValue"), "some code");
		click(By.id("save_metadata"));
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.hour.rate");
		assertTextPresent("56.34");
		assertTextPresent("accounting.code");
		assertTextPresent("some code");

		// test non hidden metadata
		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        // wait for the metadata div
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.hour.rate");
		assertTextPresent("56.34");
		assertTextPresent("accounting.code");
		assertTextPresent("some code");

		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
		// hide accounting code
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
		setFormElement(By.id("metadataKey"), "accounting.code");
		setFormElement(By.id("metadataValue"), "some code");
		click(By.id("metadataHidden"));
		click(By.id("save_metadata"));
		
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.hour.rate");
		assertTextPresent("56.34");
		assertTextPresent("accounting.code");
		assertTextPresent("some code");

		// test non hidden metadata
		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
		// wait for the metadata div
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.hour.rate");
		assertTextPresent("56.34");
		assertTextNotPresent("accounting.code");
		assertTextNotPresent("some code");
	}

	@Test
	public void testGroupMetadata() {
		// create hour rate
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
		setFormElement(By.id("metadataKey"), "accounting.hour.rate");
		setFormElement(By.id("metadataGroup"), "Sample Group");
		setFormElement(By.id("metadataValue"), "56.34");
        click(By.id("save_metadata"));
        waitForElement(By.id("add-metadata"));
        		assertTextPresent("accounting.hour.rate");
		assertTextPresent("Sample Group");
		assertTextPresent("56.34");

		// test group text visible for test user
		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.hour.rate");
		assertTextPresent("Sample Group");
		assertTextPresent("56.34");

	}

	@Test
	public void testHideTheTab_JMETA_17() {
		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP");

		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
		setFormElement(By.id("metadataKey"), "jira.metadata.projectRole.hide.tab.project");
		setFormElement(By.id("metadataValue"), "Users");
		click(By.id("save_metadata"));

		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("test", "test");
		gotoUrl("/browse/SP");
        assertFalse(jira.getTester().getDriver().elementExists(By.id("com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab-panel")));

		jira.logout();
		jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
		gotoUrl("/browse/SP");
        assertTrue(jira.getTester().getDriver().elementExists(By.id("com.osoboo.jira-metadata-plugin:JIRA-Metadata-project-tab-panel")));
	}

	@Test
	public void testCreateViewCustomField() {
		String randomKey = new StringBuilder("PK-23-Sys-3").append(UUID.randomUUID().toString()).toString();
		// pre-check
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextNotPresent("Account Info");

        // 1. custom field add
        ViewCustomFields viewCustomFields = pageBinder.navigateToAndBind(ViewCustomFields.class);
        TypeSelectionCustomFieldDialog customFieldDialog = viewCustomFields.addCustomField();
        // select all (the first entry)
        click(By.cssSelector("button.item-button"));
        customFieldDialog.select("JIRA Metadata - View project metadata field");
        ConfigureFieldDialog configureFieldDialog = customFieldDialog.next();
        configureFieldDialog.name("Account Info");
        // set the screen
        configureFieldDialog.nextAndThenAssociate();
        click(By.name("associatedScreens"));
        
        click(By.id("update_submit"));

        waitAndClick(By.className("js-default-dropdown"));
        click(By.id("config_customfield_10000"));
        waitAndClick(By.id("customfield_10000-edit-default"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By
                .id("customfield_10000"), null));
        setFormElement(By.id("customfield_10000"), "accounting.code");
        click(By.id("set_defaults_submit"));

		// issue tests
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextPresent("Account Info");
		assertTextNotPresent("accounting.code");
		assertTextNotPresent(randomKey);

		// create metadata
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
		setFormElement(By.id("metadataKey"), "accounting.code");
		setFormElement(By.id("metadataValue"), randomKey);
		click(By.id("save_metadata"));
        waitForElement(By.id("jira-metadata"));
		assertTextPresent("accounting.code");
		assertTextPresent(randomKey);
		// im Issue
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextPresent("Account Info");
		assertTextNotPresent("accounting.code");
		assertTextPresent(randomKey);
	}

	@Test
	public void testCreateCalcCustomField() {
		// pre-check
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextNotPresent("Amount");

        // 1. custom field add
        ViewCustomFields viewCustomFields = pageBinder.navigateToAndBind(ViewCustomFields.class);
        TypeSelectionCustomFieldDialog customFieldDialog = viewCustomFields.addCustomField();
        // select all (the first entry)
        click(By.cssSelector("button.item-button"));
        customFieldDialog.select("JIRA Metadata - Calc. field");
        ConfigureFieldDialog configureFieldDialog = customFieldDialog.next();
        configureFieldDialog.name("Amount");
        // set the screen
        configureFieldDialog.nextAndThenAssociate();
        click(By.name("associatedScreens"));
        click(By.id("update_submit"));
        
        waitAndClick(By.className("js-default-dropdown"));
        click(By.id("config_customfield_10000"));
        waitAndClick(By.id("customfield_10000-edit-default"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By
                .id("customfield_10000"), null));
        setFormElement(
                By.id("customfield_10000"),
                "#set( $accRate = $metadataService.getMetadataValue($issue.projectObject, 'accounting.hour.rate') ) #set( $issueTime = $math.div($issue.timeSpent, 3600) ) #set( $amount = $math.mul($accRate, $issueTime) ) $!amount");
        click(By.id("set_defaults_submit"));


        // create metadata
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "accounting.hour.rate");
        setFormElement(By.id("metadataValue"), "12.5");
        click(By.id("save_metadata"));
        waitForElement(By.id("jira-metadata"));
        assertTextPresent("accounting.hour.rate");
		assertTextPresent("12.5");

		// Issue
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextPresent("Amount");
		assertTextNotPresent("accounting.hour.rate");
		assertTextPresent("31.25");
	}

}
