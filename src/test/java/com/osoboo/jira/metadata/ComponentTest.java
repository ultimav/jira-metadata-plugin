package com.osoboo.jira.metadata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class ComponentTest extends AbstractMetadataTestCase {

    protected boolean shouldSkipSetup() {
        return true;
    }


    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("base_with_project_subtask_2013_01_29.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testCreateEditDeleteMetadata() {
        // create metadata
        gotoUrl("/browse/SP/component/10000/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab");

        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "create-edit-delete");
        setFormElement(By.id("metadataValue"), "some value");
        click(By.id("save_metadata"));
        waitForElement(By.id("add-metadata"));
        assertTextPresent("create-edit-delete");
        assertTextPresent("some value");

        // edit metadata
        waitAndClick(By.id("edit-create_edit_delete"));
        setFormElement(By.id("metadataValue"), "some other value");
        click(By.id("save_metadata"));
        waitForElement(By.id("add-metadata"));
        assertTextPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextPresent("some other value");

        // view other component
        gotoUrl("/browse/SP/component/10001/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab");
        assertTextNotPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextNotPresent("some other value");

        // go back and delete metadata
        gotoUrl("/browse/SP/component/10000/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab");
        waitAndClick(By.id("delete-create_edit_delete"));
        jira.getTester().getDriver().switchTo().alert().accept();
        gotoUrl("/browse/SP/component/10000/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab");
        assertTextNotPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextNotPresent("some other value");
    }


    @Test
    public void testHideTheTab_JMETA_17() {
        jira.logout();
        jira.gotoLoginPage().loginAndGoToHome("test", "test");
        gotoUrl("/browse/SP/component/10000");
        waitForElement(By.id("content"));

        assertTrue(jira.getTester().getDriver().elementExists(
                By.id("com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab-panel")));

        jira.logout();
        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
        gotoUrl("/browse/SP/component/10000/?selectedTab=com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab");

        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "jira.metadata.projectRole.hide.tab.component");
        setFormElement(By.id("metadataValue"), "Users");
        click(By.id("save_metadata"));

        jira.logout();
        jira.gotoLoginPage().loginAndGoToHome("test", "test");
        gotoUrl("/browse/SP/component/10000");
        waitForElement(By.id("content"));
        assertFalse(jira.getTester().getDriver().elementExists(
                By.id("com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab-panel")));

        jira.logout();
        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
        gotoUrl("/browse/SP/component/10000");
        waitForElement(By.id("content"));
        assertTrue(jira.getTester().getDriver().elementExists(
                By.id("com.osoboo.jira-metadata-plugin:JIRA-Metadata-component-tab-panel")));
    }

}
