package com.osoboo.jira.metadata;

import org.junit.Test;
import org.openqa.selenium.By;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class GroupTest extends AbstractMetadataTestCase {

    protected boolean shouldSkipSetup() {
        return true;
    }

	public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("base_with_project_subtask_2013_01_29.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
	}

	@Test
	public void testCreateEditDeleteMetadata() {
		// select user
		gotoUrl("/secure/admin/metadata/GroupAction!default.jspa");
		waitSetFormElement(By.id("selectedGroup"), "jira-users");
		click(By.id("select_group"));
		waitForElement(By.id("add-metadata"));
		assertTextPresent("There are no metadata for the group");

        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "create-edit-delete");
        setFormElement(By.id("metadataValue"), "some value");
        click(By.id("save_metadata"));

        waitForElement(By.id("add-metadata"));
        assertTextPresent("create-edit-delete");
        assertTextPresent("some value");

		
		// edit metadata
        waitAndClick(By.id("edit-create_edit_delete"));
        setFormElement(By.id("metadataValue"), "some other value");
        click(By.id("save_metadata"));
        waitForElement(By.id("add-metadata"));
        assertTextPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextPresent("some other value");

		// view other group
		gotoUrl("/secure/admin/metadata/GroupAction.jspa?selectedGroup=jira-developers");
        waitForElement(By.id("add-metadata"));
		assertTextNotPresent("create-edit-delete");
		assertTextNotPresent("some value");
		assertTextNotPresent("some other value");

		// go back and delete metadata
		gotoUrl("/secure/admin/metadata/GroupAction.jspa?selectedGroup=jira-users");
        waitAndClick(By.id("delete-create_edit_delete"));
        jira.getTester().getDriver().switchTo().alert().accept();

		gotoUrl("/secure/admin/metadata/GroupAction.jspa?selectedGroup=jira-users");
        waitForElement(By.id("add-metadata"));
		assertTextNotPresent("create-edit-delete");
		assertTextNotPresent("some value");
		assertTextNotPresent("some other value");
	}

}
